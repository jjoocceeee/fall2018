#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mode_A.h"
#include "mode_B.h"
#include "mode_C.h"

int main(int argc, char *argv[]) {
  //Flags will come first
  char* pattern;
  int t_flag = 0;  // 0-> will NOT print result   1-> WILL print result
  int mode = 0;    // 0->a,  1->b,  2->c

  for (int i = 1; i < argc; ++i){
    if(strncmp(argv[i], "-", 1) == 0){
      if(strcmp(argv[i],"-a") == 0){
        mode = 0;
      } else if(strcmp(argv[i], "-b") == 0){
        mode = 1;
      } else if(strcmp(argv[i], "-c") == 0){
        mode = 2;
      } else if(strcmp(argv[i], "-t") == 0){
        t_flag = 1;
      }
    } else {
      //it is a pattern to look at.
      // printf("Pattern:  %s\n", argv[i]);
      // printf("MODE: %d\n", mode);
      // printf("T_FLAG: %d\n", t_flag);
      if(mode == 0 ){
        char* result = mode_A(argv[i]);
        // printf("Mode A. Result %s\n", result);

        if(t_flag == 1){
          //Do nothing
          if(result!= NULL){
            printf("%s\n", result);
          }
        } else {
              if(result != NULL) {
              printf("yes\n");
              }else{
                printf("no\n");
              }
          }
      } else if(mode == 1) {
        char* result = mode_B(argv[i]);
        // printf("Mode B. Result %s\n", result);

        if(t_flag == 1) 
        {
          //Do nothing
          if(result != NULL)
          {
            printf("%s\n", result);
          } 
        }
        else {
              if(result != NULL) {
              printf("yes\n");
              }
              else {
                printf("no\n");
              }
        }
      } 
        else {
          //Mode = 2 -> C
          char* result = mode_C(argv[i]);
          // printf("Mode C. Result %s\n", result);

          if(t_flag == 1)
          {
            //Do nothing
            if(result != NULL )
            {
            printf("%s\n", result);
            } 
          } else {
              if(result != NULL) 
              {
              printf("yes\n");
              }
              else {
                printf("no\n");
              }
            } 

          }
      }
    }
  return 0;

}




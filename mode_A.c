#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mode_A.h"
#include "helper.h"


//PARAMETERS: 
//      char* pattern   - pointer to the pattern entered at the command line.
char* mode_A (char* pattern){
    char characters[] = "0123456789:_qf";
    int checking = check_all(characters, pattern);
    int ff = find_letter_repeats(pattern, 'f', 1, 5, 0);
    if(ff == 0){
        return 0;
    }
    int colon = find_letter_repeats(pattern, ':', 2, 2, ff);
    if(colon == 0){
        return 0;
    }
    int qq = find_letter_repeats(pattern, 'q', 3, 7, colon);
    if(qq == 0){
        return 0;
    }
    int under = find_letter_repeats(pattern, '_', 2, 2, qq);
    if(under == 0)
    {
        return 0;
    }
    int correct_digits = decimal_digits(pattern, 1, 3, under);
    if(correct_digits == 0){
        return 0;
    }
    char* return_val;
    return_val = Update_numbers(pattern);
    // printf("UPDATED PATTERN: %s\n", return_val);

    //DEBUGGING
    // printf("Checking all characters %d\n", checking);
    // printf("Corrent number of f's: %d\n", ff );
    // printf("Corrent number of colons: %d\n", colon);
    // printf("Correct number of q's: %d\n", qq);
    // printf("Corrent number of underscores: %d\n", under);
    // printf("Correct number of digits: %d\n", correct_digits);
    // printf("RETURN: %d\n", ff & qq & correct_digits & under);
    return return_val;
}


//Increments all decimals numbers (0-8).
//PARAMETERS:
//      char* patter    - point to the pattern entered at the command line
//RETURN:
//      char*           - An updated pointer to the pattern with 
char* Update_numbers(char* pattern){    
    for(int i = 0; i<strlen(pattern); i++){
        if((pattern[i] >= '0') && (pattern[i] <= '8'))
        {
            pattern[i] ++;
        }
    }
    return pattern;
}



    


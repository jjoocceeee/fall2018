#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mode_B.h"
#include "helper.h"


/* REQUIREMENTS:
any even number (including zero) repetitions of the letter “g”;
exactly one “_”;
between 1 and 3 (inclusive) decimal digits — call this sequence X;
between 3 and 5 repetitions (inclusive) of the letter “x”;
exactly two “=”s;
the same character sequence as X, but reversed; and
an odd number of uppercase letters.


For matches, perform the following conversion:
after each character, insert a digit that is the position of the character modulo 8.*/

//PARAMETERS: 
//      char* pattern   - pointer to the pattern entered at the command line.
char* mode_B (char* pattern){
    char characters[] = "0123456789:xg=_ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    int checking = check_all(characters, pattern);
    // printf("Checking all characters %d\n", checking);

    if(checking == 0)
    {
      return 0;
    }

    int g = find_letter_repeats_even(pattern, 'g', 0);
    if(g == -1){
      return 0;
    }
    int underscore = find_letter_repeats(pattern, '_', 1, 1, g);

    if(underscore == 0){
      return 0;
    }
    char* getting_x = get_x(pattern);
    // printf("X: %s\n", getting_x);

    if(strlen(getting_x) <1 | strlen(getting_x) > 3) {
      return 0;
    }
    
    int xx = find_letter_repeats(pattern, 'x', 3, 5, underscore + strlen(getting_x));

    if(xx == 0){
      return 0;
    }
    int equal = find_letter_repeats(pattern, '=', 2, 2, xx);

    if(equal == 0){
      return 0;
    }
    int reversed_pattern = reverse_x(pattern, getting_x, equal);

    if(reversed_pattern == 0){
      return 0;
    }
    int  cl = capital_letters(pattern);

    if(cl == 0){
      return 0;
    }

    return Update_numbers_b(pattern);
}



/* Counts the number of capital letters.
 * PARAMETERS:  pattern
 * RETURNS:     1 -> Odd number of capital letters
 *              0 -> Even number of capital letters
 */
int capital_letters(char* pattern){  
  int count = 0;
  int index = 0;
  int first = 0;
  for(int i = 0; i<strlen(pattern); i++){
    //Finding first occurance of a capital letter.
    if((pattern[i] >= 'A') && (pattern[i] <= 'Z')) {
     first = i;
     count ++;
    }
  }

  //preparing return statement.
  if((count & 1) == 0)
    return 0;
  else
    return 1;
}


int reverse_x(char* pattern, char* x, int starting){
  //Finding first location of first item in pattern.
  char* first;
  int first_index;
  int size_of_x = strlen(x);
  int countdown = size_of_x - 1;
  int second = 0;

  int index = starting;           //getting location of first occurance


  if(pattern[index] == x[countdown]){
    //testing backwards pattern
    for(int i = index; i < index + size_of_x; i++){
      // printf("Entering for loop testing second pattern\n");
      if(pattern[i] != x[countdown]){
        // printf("pattern[i]: %c at location %d doesn't match x[countdown]: %c at location %d\n", pattern[i], i, x[countdown], countdown);
        return 0;
      }
        // printf("pattern[i]: %c at location %d MATCHES x[countdown]: %c at location %d\n", pattern[i], i, x[countdown], countdown);
        countdown --;
    }
  }
  return index;
}



//Will find the location of the first f in the pattern.
//PARAMETERS:
//      char* pattern   - pointer to the pattern entered at the command line
//      char  letter    - letter searching for by the pattern. 
int find_letter_repeats_even(char* pattern, char letter, int starting) {
      // printf("Starting at: %d\n", starting);
    char *ret;
    int index = starting;
    int count_of_letter = 0;

    if(pattern[starting] == letter){
        //The letter does exist in the word
        count_of_letter = 1;
        index++;
        //Move along the next letter and check if it is the same.
        while(pattern[index] == letter){
            count_of_letter ++;
            index++;
        }
    }
  //preparing return statement.
  if((count_of_letter & 1) == 0){
    //even
    return index;
  }
  else {
    return -1;         //Odd
  }
}

//PARAMETERS:
//      char* pattern    - point to the pattern entered at the command line
//RETURN:
//      char*           - An updated pointer to the pattern containing the resulting array.
char* Update_numbers_b(char* pattern){    
    int second_index = 0;
    char* return_array;
    //creatingan array that is 2 times the size.
    return_array = (char *)malloc(strlen(pattern)*2); 
    //return_array[strlen(pattern)*2] = 0x00;
    //setting escape character at end of array.
    //WIll get the index of the first and last H.
    for(int i = 0; i<strlen(pattern); i++){
      return_array[second_index] = pattern[i];
      return_array[second_index + 1] = (i % 8)+'0';
      second_index += 2;
    }
    return return_array;
}
#ifndef HELPER
#define HELPER
int check_all(char*, char*);
int total_characters(char*, char, int, int, int);
int decimal_digits(char*, int, int, int);
int find_letter_repeats(char*, char,int, int, int);
char* get_x(char*);
char* get_x_letters(char*, int);



char* mode_A(char*);
int find_letter_repeats(char*, char,int, int, int);
int underscore(char*, char, int, int);
char* Update_numbers(char*);


char* mode_B (char*);
int capital_letters(char*);
int find_letter_repeats_even(char*, char, int);
int reverse_x(char*, char*, int);
char* Update_numbers_b(char*);
#endif
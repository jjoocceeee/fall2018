#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Will find the location of the first f in the pattern.
//PARAMETERS:
//      char* pattern   - pointer to the pattern entered at the command line
//      char  letter    - letter searching for by the pattern. 
//RETURNS:
//        int             - The index where the pattern left off
int find_letter_repeats(char* pattern, char letter,int min_amount, int max_amount, int starting){
    // printf("Starting at: %d\n", starting);
    char *ret;
    int index = starting;
    int count_of_letter = 0;

    if(pattern[starting] == letter){
        //The letter does exist in the word
        count_of_letter = 1;
        index++;
        //Move along the next letter and check if it is the same.
        while(pattern[index] == letter){
            count_of_letter ++;
            index++;
        }
    }

    //preparing return statement.
    if(count_of_letter < min_amount || count_of_letter > max_amount){
        return 0;
    }
    return index;           //OK
}


int check_all(char* allowed_characters, char* pattern){
   
    for(int i = 0; i<strlen(pattern); i++){
        char* chars = strchr(allowed_characters, pattern[i]);
        int index;        
        index = (int)(chars - pattern);
        if((strchr(allowed_characters, pattern[i]) == NULL)){
            return 0;
        }
    }
    return 1;
}



//Will find the location of the first f in the pattern.
//PARAMETERS:
//      char* pattern   - pointer to the pattern entered at the command line
//      char  letter    - letter searching for by the pattern. 
//      int min_amount  - 
//      int max_amount  - 
//      int starting    - 
//RETURNS:
//        int             - The index where the pattern left off
int total_characters(char* pattern, char letter, int min_amount, int max_amount, int starting){
    // printf("Starting at: %d\n", starting);

    int count = 0;
    int index = starting;
    for(int i = index; i<strlen(pattern); i++){
        if(pattern[i] == letter) {
            count ++;
            index = i;
        }
    }

    //preparing return statement.
    if(count < min_amount || count > max_amount){
        return 0;
    }
    return index;           //OK
}

/* Counts the number of decimal digits in the pattern.
 * PARAMETERS:      pattern      -> pattern of count capital letters.
 *                  min_ammount  -> Minimum number of capital letters.
 *                  max_ammount  -> Maximum number of capital letters.
 * RETURNS:         0 -> If there is a correct number of decimal digits.
 *                  1 -> If incorrect number of decimal digits.
 */
int decimal_digits(char* pattern, int min_amount, int max_amount, int starting){
    // printf("Starting at: %d\n", starting);

    int count = 0;
    int index = 0;
    for(int i = starting; i<strlen(pattern); i++){
        if((pattern[i] >= '0') && (pattern[i] <= '9'))
        {
            count ++;
            index = i;
        }
    }

    //preparing return statement.
    if(count < min_amount || count > max_amount){
        return 0;
    }
    return index;           //OK
}


char* get_x(char* pattern){
  int count = 0;
  int index = 0;
  int first = 0;
  char* ret;
  char* temp_array;

  //Counting the number of occurances after the first occurance.
  for(int i = 0; i<strlen(pattern); i++){
    if((pattern[i] >= '0') && (pattern[i] <= '9')) {
      temp_array = (char *)malloc(count + 1);
      ret = temp_array;
      ret[count] = pattern[i];
      count ++;
      first = i;
      break;
    }
  }

  index = first + 1;
  while(pattern[index] >= '0' && pattern[index] <= '9'){
    temp_array = (char *)realloc(ret, count + 1);
    ret = temp_array;
    ret[count] = pattern[index];
    count++;
    index++;
  }

//   printf("Checking string size of array: %d\n", strlen(ret));
  return ret;
}

char* get_x_letters(char* pattern, int starting){
  int count = 0;
  int index = 0;
  int first = 0;
  char* ret;
  char* temp_array;
    if(pattern[starting] < 'A' && pattern[starting] > 'Z'){
        // printf("location: %d, is NOT a capital letter [%c].\n", starting, pattern[starting]);
        return NULL;
    }

    index = starting;
  //Counting the number of occurances after the first occurance.
  for(int i = index; i<strlen(pattern); i++){
    if((pattern[i] >= 'A') && (pattern[i] <= 'Z')) {
      temp_array = (char *)malloc(count + 1);
      ret = temp_array;
      ret[count] = pattern[i];
      count ++;
      index = i;
      break;
    }
  }

  index ++;
  while(pattern[index] >= 'A' && pattern[index] <= 'Z'){
    temp_array = (char *)realloc(ret, count + 1);
    ret = temp_array;
    ret[count] = pattern[index];
    count++;
    index++;
  }

//   printf("Checking string size of array: %d\n", strlen(ret));
  return ret;
}
#ifndef MODE_B
#define MODE_B
char* mode_B (char*);
int capital_letters(char*);
int find_letter_repeats_even(char*, char, int);
int reverse_x(char*, char*, int);
char* Update_numbers_b(char*);
#endif
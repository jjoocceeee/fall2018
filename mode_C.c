#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mode_B.h"
#include "mode_C.h"
#include "helper.h"

/* REQUIREMENTS:
any even number (including zero) repetitions of the letter “g”;
exactly one “_”;
an odd number of uppercase letters — call this sequence X;
between 2 and 5 repetitions (inclusive) of the letter “v”;
exactly one “,”;
between 1 and 3 (inclusive) decimal digits; and
the same characters as the odd-positioned characters in X.

For matches, perform the following conversion:
after each character, insert a digit that is the position of the character modulo 8.*/

//PARAMETERS: 
//      char* pattern   - pointer to the pattern entered at the command line.
char* mode_C (char* pattern){
    char characters[] = "0123456789:vg_,ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    int checking = check_all(characters, pattern);
    // printf("Checking all characters %d\n", checking);
    if(checking == 0)
    {
    //   printf("Exited because checking found unwanted characters\n");
      return 0;
    }

    int g = find_letter_repeats_even(pattern, 'g', 0);
    // printf("Correct number of g: %d\n", g);
    if(g == -1){
    //   printf("Exited because g returned: %d", g);
      return 0;
    }
    int underscore = find_letter_repeats(pattern, '_', 1, 1, g);
    // printf("Corrent number of underscore: %d\n", underscore);

    if(underscore == 0){
    //   printf("Exited because underscore returned: %d\n", underscore);
      return 0;
    }
    char* getting_x = get_x_letters(pattern, underscore);
    // printf("X: %s\n", getting_x);

    if(getting_x == NULL){
        // printf("gettingX NULL!\n");
        return 0;
    }

    if(strlen(getting_x) & 1 != 1) {
    //   printf("Exited because getting_x returned: %s\n", getting_x);
      return 0;
    }
    
    int vv = find_letter_repeats(pattern, 'v', 2, 5, underscore + strlen(getting_x));
    // printf("Correct number of v's: %d\n", vv);

    // printf("LOOKING AT: %d index after the pattern\n",  underscore + strlen(getting_x));
    if(vv == 0){
    //   printf("Exited because vv !!!returned: %d!!!!!!\n", vv);
      return 0;
    }
    int comma = find_letter_repeats(pattern, ',', 1, 1, vv);
    // printf("Corrent number of equals: %d\n", comma);
    if(comma == 0){
    //   printf("Exited because equal returned: %d\n", comma);
      return 0;
    }
    int correct_digits = decimal_digits(pattern, 1, 3, comma);
    if(correct_digits == 0){
    //   printf("Exited because equal returned: %d\n", correct_digits);
      return 0;
    }
    
    int next_odd = reverse_x_modec(pattern, getting_x, correct_digits);
    // printf("ODD X: %d\n", next_odd);

    if(next_odd == 0){
    //   printf("Exited because reversed_pattern returned: %d\n", next_odd);
      return 0;
    }

    return Update_numbers_c(pattern);
    
}


int reverse_x_modec(char* pattern, char* x, int starting){

  int size_of_x = strlen(x);
  int second = 0;

  int index = starting;           //getting location of first occurance

    int next_odd = 1;
  if(pattern[index] == x[next_odd]){
    //testing backwards pattern
    while(next_odd <= strlen(x)){
      // printf("Entering for loop testing second pattern\n");
      if(pattern[index] != x[next_odd]){
        // printf("pattern[index]: %c at location %d doesn't match x[next_odd]: %c at location %d\n", pattern[index], index, x[next_odd], next_odd);
        return 0;
      }
        // printf("pattern[index]: %c at location %d MATCHES x[next_odd]: %c at location %d\n", pattern[index], index, x[next_odd], next_odd);
        next_odd +=2;
        index ++;
    }
  }
  return index;
}




//Increments all decimals numbers (0-8).
//PARAMETERS:
//      char* patter    - point to the pattern entered at the command line
//RETURN:
//      char*           - An updated pointer to the pattern with 
char* Update_numbers_c(char* pattern){    
    int first_H = -1;
    int last_H = -1;
    char* return_array;
    return_array = (char *)malloc(strlen(pattern));


    //WIll get the index of the first and last H.
    for(int i = 0; i<strlen(pattern); i++){
        if(pattern[i] == 'H') {
            if(first_H == -1) {
                first_H = i;
            } else {
                last_H = i;
            }
        }
    }
    int location = 0;
    for(int i = 0; i< strlen(pattern); i++){
        if(i != first_H && i != last_H) {
            return_array[location] = pattern[i];
            location ++;
        }
    }
    return return_array;
}